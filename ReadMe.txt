
TUTORIAL
========

	WADL-first approach on how to create a CXF REST clients and servers
	(from scratch, using a text editor, all based on Fuse 6.2.1)

	Index:
	-----
		1. REST-Server Simple-data in Fuse
		2. Running standalone (and testing)
		3. REST-Server Complex-data (XML) in Fuse
		4. REST-Server data validation (XML)
		5. REST-Client Simple-data in Fuse
		6. REST-Client Complex-data (XML) in Fuse
		7. JSON REST-Server
		8. JSON REST-Client


	And learn interesting bits like:
	--------------------------------
		- enable XML/JSON validation against XSDs with CXF
		- use Camel's Simple language to access Java fields
		- construct JAXB data structures with beans in Blueprint
		- JSON/XML namespaces mapping



1. REST-Server Implementation and Deployment
   (and testing using SoapUI)
=================================

1.1 Create skeleton

	mvn archetype:generate                   \
	  -DarchetypeGroupId=org.apache.camel.archetypes  \
	  -DarchetypeArtifactId=camel-archetype-blueprint \
	  -DarchetypeVersion=2.15.1.redhat-621084  \
	  -DgroupId=learn.rest                  \
	  -DartifactId=rest-cxf-wf

	(there is a bug in the JUnit, remove the slash in "/OSGI-INF...")

	Note: the archetype will generate some code that may enter in conflict
		  with our tutorial, simply remove the non-relevant code.


1.2 Create a WADL and place under:

	> src/main/resources/wadl

		<application xmlns="http://wadl.dev.java.net/2009/02" xmlns:xs="http://www.w3.org/2001/XMLSchema">
			<grammars/>
			<resources base="http://localhost:10000">
				<resource path="/say">
					<resource path="hello/{queryString}">
						<param name="queryString" style="template" type="xs:string"/>
						<method name="GET">
							<request/>
							<response>
								<representation mediaType="*/*">
									<param name="result" style="plain" type="xs:string"/>
								</representation>
							</response>
						</method>
					</resource>
				</resource>
			</resources>
		</application>


1.3 Add the WADL2java plugin to generate the sources

  (ref: http://cxf.apache.org/docs/jaxrs-services-description.html#JAXRSServicesDescription-WADLoverview)

	note: the property 'cxf.version' is set to '3.0.4.redhat-621084'
	note: the sources will be generated in the 'target', this ensure 'src' stays clean.
	note: the plugin requires the dependency 'camel-cxf' to load the jax-rs api classes.

		...
		  <camel.version>2.15.1.redhat-621084</camel.version>
		  <cxf.version>3.0.4.redhat-621084</cxf.version>
		</properties>
		...
		<dependency>
		  <groupId>org.apache.camel</groupId>
		  <artifactId>camel-cxf</artifactId>
		  <version>${camel.version}</version>
		</dependency>
		...
		<plugin>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-wadl2java-plugin</artifactId>
			<version>${cxf.version}</version>
			<executions>
				<execution>
				    <id>generate-sources</id>
				    <phase>generate-sources</phase>
				    <configuration>
				        <sourceRoot>${basedir}/target/generated/src/main/java</sourceRoot>
				        <wadlOptions>
				            <wadlOption>
				                <wadl>${basedir}/src/main/resources/wadl/hello.xml</wadl>
				                <packagename>learn.rest</packagename>		                     
				            </wadlOption>
				        </wadlOptions>
				    </configuration>
				    <goals>
				        <goal>wadl2java</goal>
				    </goals>
				</execution>
			</executions>
		</plugin>


1.4 Add the endpoint in Blueprint

	a. Add the namespace:  
		xmlns:cxf="http://camel.apache.org/schema/blueprint/cxf"

	b. and the endpoint:

		<cxf:rsServer id="mycxfrest"
			address="http://localhost:10000"
			serviceClass="learn.rest.SayResource"/>

		Notes about 'address':
			- its value will overwrite the one defined in the WSDL's port.
			- it requires 'host:port' if run standalone (when testing or with 'camel:run')
			- when deployed in Fuse
				> if missing, CXF will assign a port (and ignore the one from the WADL)
				   - e.g. http://localhost:44412
				> when defined as 'address="/sc1' (no 'host:port') then it will default to:
				   - http://localhost:8181/cxf/sc1


1.5 Add the Camel CXF route to consume traffic

	<route id="my-cxf-route">
		<from uri="cxfrs:bean:mycxfrest"/>
		<setBody>
			<simple>Hello ${body}</simple>
		</setBody>
	</route>


1.6 Install in Maven and deploy in Fuse (and start)

	a. mvn clean install -DskipTests=true

	b. JBossFuse:karaf@root> install -s mvn:learn.rest/rest-cxf-wf/1.0.0


1.7 From a browser, check it got deployed:

	With the address:
	http://localhost:8181/cxf

	It should show:
	Available RESTful services:
		Endpoint address: http://localhost:10000
		WADL : http://localhost:10000?_wadl


1.8 From SoapUI, you should be able to test it:

	a. Select: File > New REST Project
	b. Press the button 'Import WADL...' and enter:
		> http://localhost:10000?_wadl
	c. In the 'resource' textbox replace the value '{queryString}'
	d. trigger the request and you should get the response back.


2. Testing JUnits and running standalone
========================================

	JBoss Fuse (the engine) already contains all the dependencies required by the project.
	But the project won't run without a number of dependencies when:
		a. running JUnits (maven test phase)
		b. running standalone (with 'mvn camel:run')


2.1 The POM file needs some CXF dependencies:

	a. add dependency ASM at the top of <dependencies>

		<dependency>
		  <groupId>org.ow2.asm</groupId>
		  <artifactId>asm-all</artifactId>
		  <version>4.1</version>
		</dependency>

		(ref: https://access.redhat.com/solutions/1128593)

	b. add CXF dependencies:

		<dependency>
		    <groupId>org.apache.cxf</groupId>
		    <artifactId>cxf-rt-transports-http</artifactId>
		    <version>${cxf.version}</version>
		</dependency>
		<dependency>
		    <groupId>org.apache.cxf</groupId>
		    <artifactId>cxf-rt-transports-http-jetty</artifactId>
		    <version>${cxf.version}</version>
		</dependency>

		(ref: http://cxf.apache.org/docs/using-cxf-with-maven.html)

2.2 To run standalone:

	a. run the command:
		> mvn clean camel:run

	b. from SoapUI you should be able to hit the service via the URL:
		> http://localhost:10000/say/hello/{queryString}
	   e.g.
		> http://localhost:10000/say/hello/world

	Note: there seems to be a bug as the WADL seems not available via:
		> http://localhost:10000?_wadl


2.3 To run Junits

	a. add the following JUnit that uses the CXF endpoint to send a WS request:

			@Test
			public void testRestCxfServer() throws Exception{
				String uri = "netty-http:http://localhost:10000/say/hello/junit";
				String response = template.requestBody(uri, null, String.class);
				assertEquals("ups", "Hello junit", response);
			}

		Note: the body sent is 'null', this ensures Netty uses the HTTP GET method on the request.

	   The Junit makes use of Camel Netty HTTP, therefore the following Maven dependendy needs to be added:

			<dependency>
			  <groupId>org.apache.camel</groupId>
			  <artifactId>camel-netty-http</artifactId>
			  <version>${camel.version}</version>
			</dependency>

	b. run the maven command:
		> mvn clean test



3. Complex-Data REST-Server Implementation
==========================================

	This section explains how to define a complex-data (XML) REST service.
	The example that follows is based in the previous sections.
	The interface will be extended to accept XML data in the HTTP payload.

3.1 Extend the WADL definition with a new method

	a) import an XSD definition (top of WADL)

		<grammars>
		  <include href="../xsd/Hello.xsd"/>
		</grammars>

	   where the XSD would define the following elements:

		  <xs:element name="hello">
			<xs:complexType>
			  <xs:sequence>
				<xs:element minOccurs="0" name="title" type="xs:string"/>
				<xs:element minOccurs="0" name="name" type="tns:name"/>
			  </xs:sequence>
			</xs:complexType>
		  </xs:element>

		  <xs:complexType name="name">
			<xs:sequence>
			  <xs:element minOccurs="0" name="given" type="xs:string"/>
			  <xs:element minOccurs="0" name="family" type="xs:string"/>
			</xs:sequence>
		  </xs:complexType>


		Important:
		> Things you should know when converting an XSD to JAVA:
			ref: http://blog.bdoughan.com/2012/07/jaxb-and-root-elements.html
		> Not understanding JAXB rules may get you in trouble with errors like:
			"missing an @XmlRootElement annotation"


	b) Include a new WADL resource as a POST method

			<resource path="hi">
				<method name="POST">
					<request>
						<representation element="hi:hello" mediaType="text/xml"/>
					</request>
					<response>
						<representation mediaType="*/*">
							<param name="result" style="plain" type="xs:string"/>
						</representation>
					</response>
				</method>
			</resource>

		Note the new method references the XSD element 'hi:hello'
		where the namespace 'hi' is defined as:
			> xmlns:hi="http://cxf.learn/"

		The new resource will allow clients to POST XML to the following URI:

			> http://{host:port}/say/hi


3.2 To run standalone:

	a. run the command:
		> mvn clean camel:run

	b. from SoapUI you should be able to hit the service via the URL:
		> http://localhost:10000/say/hi

	   posting the following XML:

			<tns:hello xmlns:tns="http://cxf.learn/">
				<title>Mr.</title>
				<name>
					<given>Client</given>
					<family>Eastwood</family>
				</name>
			</tns:hello>
 


4. REST-Server Data Validation
==============================

	In the previous chapter a new XML data service was defined and tested.
	But there was a catch, the REST-server is not fully validating the input data.

	- The input "Not XML"
	  would throw the following error:
		> JAXBException occurred : Unexpected character 'N' (code 78) in prolog; expected '<'

	- The input "<random/>"
	  would throw the following error:
		> JAXBException occurred : unexpected element (uri:"", local:"random")


	All the above are expected behaviours, however

	- The input:

		<tns:hello xmlns:tns="http://cxf.learn/">
			<random/>
		</tns:hello>

	  would be accepted by the REST server but shouldn't.

	***************************************************************
	* Warning: The following solution works running standalone, but
	*          at the moment this document was written it appeared 
	*          not to work when deployed in Fuse. A support case
	*          was opened to address the issue (Case 01637554).
	***************************************************************

	The following section shows how to enforce data validation. 


4.1 Include a new REST server configuration as follows:

	  <cxf:rsServer id="restvalidation"
		 address="http://localhost:20000"
		 serviceClass="learn.rest.SayResource">
			<cxf:providers>
				<ref component-id="jaxbProvider"/>
			</cxf:providers>
	  </cxf:rsServer>

	  <bean id="jaxbProvider" class="org.apache.cxf.jaxrs.provider.JAXBElementProvider">
		<property name="schemaLocations" ref="myschemas"/>
	  </bean>

	  <bean id="myschemas" class="java.util.ArrayList">
		<argument>
		  <list>
			<value>xsd/Hello.xsd</value>
		  </list>
		</argument>
	  </bean>

	This configuration includes a JAXB Provider that loads the XSD to validate against.

	Please note my comment in the following knowledge-base article:
		> https://access.redhat.com/solutions/1562263


4.2 Include a new Camel route to consume traffic based on the above endpoint configuration

		<route id="my-cxf-route-data-validation">
			<from uri="cxfrs:bean:restvalidation"/>
			<setBody>
				<simple>Hello ${body[0].title} ${body[0].name.given} ${body[0].name.family}</simple>
			</setBody>
		</route>

	Note how Camel's Simple language allows access to java values. 


4.3 To run standalone:

	a. run the command:
		> mvn clean camel:run

	b. from SoapUI you should be able to hit the service via the URL:
		> http://localhost:20000/say/hi

	c. posting the following XML:

			<tns:hello xmlns:tns="http://cxf.learn/">
				<random/>
			</tns:hello>

		should be rejected with:

			"Invalid content was found starting with element 'random'."
	
	d. posting the following XML:

			<tns:hello xmlns:tns="http://cxf.learn/">
				<title>Mr.</title>
				<name>
					<given>Client</given>
					<family>Eastwood</family>
				</name>
			</tns:hello>

		should return:

			"Hi Mr. Client Eastwood"


4.4 Include JUnits to validate implementation

	a. The following JUnit ensures invalid XML is rejected

			@Test
			public void testRestCxfServerComplexDataValidationRejectsInput() throws Exception{
				String message = null;
				try
				{	//compose and trigger the request
					String response = template.requestBodyAndHeader(
						"netty-http:http://localhost:20000/say/hi", 
						"<tns:hello  xmlns:tns=\"http://cxf.learn/\"><random/></tns:hello>", 
						"Content-Type", "text/xml", 
						String.class);
				}
				catch(Exception e){message = e.getMessage();};
				assertTrue("ups", message.contains("Invalid content was found starting with element 'random'"));
			} 


	b. The following JUnit ensures valid XML is accepted

			@Test
			public void testRestCxfServerComplexDataValidationAcceptsInput() throws Exception{
				String validXml = 
					"<tns:hello xmlns:tns=\"http://cxf.learn/\">"+
					"	<title>Mr.</title>"+
					"	<name>"+
					"		<given>Client</given>"+
					"		<family>Eastwood</family>"+
					"	</name>"+
					"</tns:hello>";

				String response = template.requestBodyAndHeader(
					"netty-http:http://localhost:20000/say/hi", 
					validXml, 
					"Content-Type", "text/xml", 
					String.class);

				assertEquals("ups", "Hi Mr. Client Eastwood", response);
			}



5. REST-Client (simple-data) Implementation 
=========================================== 

	The following example starts by creating the JUnit first.
	The Junit will trigger the Camel route that implements the CXFRS client invocation.


5.1 Add a new JUnit to test the REST-Client

		@Test
		public void testRestCxfClient() throws Exception{
			String response = template.requestBody("direct:trigger-cxfrs-client", null, String.class);
			assertEquals("ups", "Hello Mr. Client Eastwood", response);
		}

	The Camel route triggered is defined below.

5.2 Add the endpoint in Blueprint

		<cxf:rsClient id="myrestclient"
			address="http://localhost:10000"
			serviceClass="learn.rest.SayResource"/>


5.3 And add the following Camel route that implements the client invocation

	There are some preparations to do before handing over to the CXF client.
		> enable the use of 'Proxy-based API', to ensure the call is constructed based on the WADL defintion.
		> set the REST operation to invoke
		> set in the body the operation parameters (the 'queryString')

	(refs: 
		- http://camel.apache.org/cxfrs.html
		- https://cwiki.apache.org/confluence/display/CXF20DOC/JAX-RS+Client+API#JAX-RSClientAPI-Proxy-basedAPI)

		<route id="trigger-cxfrs-client">
		  <from uri="direct:trigger-cxfrs-client"/>
		  <setHeader headerName="CamelCxfRsUsingHttpAPI">
			<constant>false</constant>
		  </setHeader>
		  <setHeader headerName="operationName">
			<constant>getHelloqueryString</constant>
		  </setHeader>
		  <setBody>
		    <simple>Mr. Client Eastwood</simple>
		  </setBody>
		  <to uri="cxfrs:bean:myrestclient"/>
		</route>


5.4 run the Junit using the maven command:

		> mvn clean test


5.5 Alternative CXF REST-Client

	The 'rsClient' definition could be skipped, the Camel CXFRS component allows invocations without it.
	See the JUnit example below.

		@Test
		public void testRestCxfClientWithHostPort() throws Exception
		{
			String uri = "cxfrs:http://localhost:10000?resourceClasses=learn.rest.SayResource";
			DefaultExchange request = new DefaultExchange(context);
			request.setPattern(ExchangePattern.InOut);
			request.getIn().setHeader(CxfConstants.OPERATION_NAME, "getHelloqueryString");
			request.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.FALSE);
			request.getIn().setBody("Mr. Client Eastwood");
			Exchange response = template.send(uri, request);
			assertEquals("ups", "Hello Mr. Client Eastwood", response.getOut().getBody(String.class));
		}

	Note the URI's string where 'cxfrs' is used without referencing the 'rsClient' bean.
		> host and port are included in the URI
		> the parameter 'resourceClasses' indicates the Interface to use

	The preparations are very similar to the previous example:
		> using the 'DefaultExchange' in the JUnit we need to set the InOut pattern.
		> enable the use of 'Proxy-based API', to ensure the call is constructed based on the WADL defintion.
		> specify the REST operation to invoke
		> set in the body the operation parameters (the 'queryString')




6. REST-Client (complex-data) Implementation 
============================================

	This example is based on the REST-Server complex-data service 'postHi' created earlier.
	The REST-Client will send XML and the REST-Server will consume and respond.

	The implementation starts with the creation of a JUnit.
	The Junit will trigger the Camel route that executes the CXFRS client invocation.


6.1 Add a new JUnit to test the REST-Client

		@Test
		public void testRestCxfClientComplexDataXmlDsl() throws Exception{
			String response = template.requestBody("direct:trigger-cxfrs-client-complex-data", null, String.class);
			assertEquals("ups", "Hi Mr. Client Eastwood", response);
		}

	The Camel route triggered is defined below.


6.2 Include the following Bean definition (payload)

	  <bean id="hello" class="learn.cxf.Hello">
		<property name="title" value="Mr."/>
		<property name="name">
		  <bean class="learn.cxf.Name">
			<property name="given" value="Client"/>
			<property name="family" value="Eastwood"/>
		  </bean>
		</property>
	  </bean>


6.3 And add the following Camel route implementing the client invocation

	There are some preparations to do before handing over to the CXF client.
		> enable the use of 'Proxy-based API', to ensure the call is constructed based on the WADL defintion.
		> set the REST operation to invoke
		> set in the body the operation parameters (the bean 'hello')

	(refs: 
		- http://camel.apache.org/cxfrs.html
		- https://cwiki.apache.org/confluence/display/CXF20DOC/JAX-RS+Client+API#JAX-RSClientAPI-Proxy-basedAPI)

		<route id="trigger-cxfrs-client-complex-data">
		  <from uri="direct:trigger-cxfrs-client-complex-data"/>
		  <setHeader headerName="CamelCxfRsUsingHttpAPI">
			<constant>false</constant>
		  </setHeader>
		  <setHeader headerName="operationName">
			<constant>postHi</constant>
		  </setHeader>
		  <setBody>
			<simple>ref:hello</simple>
		  </setBody>		  
		  <to uri="cxfrs:http://localhost:20000?resourceClasses=learn.rest.SayResource"/>
		</route>

	Note:
		- how the body is set by referencing the Bean definition 'hello'.
		- how the invocation points to the rsServer with XML validation (port 20000).


6.4 run the Junit using the maven command:

		> mvn clean test


6.5 Java Equivalent CXF REST-Client

	This JUnit implements in Java the equivalent logic to the XML DSL one previously shown.

	@Test
	public void testRestCxfClientComplexDataWithJava() throws Exception
	{
		String uri = "cxfrs:http://localhost:20000?resourceClasses=learn.rest.SayResource";
		DefaultExchange request = new DefaultExchange(context);
		request.setPattern(ExchangePattern.InOut);
		request.getIn().setHeader(CxfConstants.OPERATION_NAME, "postHi");
		request.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.FALSE);
		Name name = new Name();
		name.setGiven("Client");
		name.setFamily("Eastwood");
		Hello hello = new Hello();
		hello.setTitle("Mr.");
		hello.setName(name);
		request.getIn().setBody(hello);
		Exchange response = template.send(uri, request);
		assertEquals("ups", "Hi Mr. Client Eastwood", response.getOut().getBody(String.class));
	}

	Note the URI used is equivalent to the one in XML DSL pointing to port 20000.

	The preparations are very similar to the previous example:
		> using the 'DefaultExchange' in the JUnit we need to set the InOut pattern.
		> enable the use of 'Proxy-based API', to ensure the call is constructed based on the WADL defintion.
		> specify the REST operation to invoke
		> set in the body the JAXB object corresponding to the WADL/XSD definition



7. JSON REST-Server Implementation
==================================

	Here it is shown how to enable 'CXFRS' to consume JSON data.	
	The example re-uses the same WADL and XSD definitions from the previous chapters.
	The new REST-Server will expect JSON data compliant to the data structure definitions. 


7.1 Redefine the WADL POST service 'hi' to include the media type 'application/json'

		Previously defined as:
			
			<request>
				<representation element="hi:hello" mediaType="text/xml"/>
			</request>

		Should now be defined as:

			<request>
				<representation element="hi:hello" mediaType="text/xml,application/json"/>
			</request>

		This will now allow clients to POST both XML and JSON, still using the same URI:

			> http://{host:port}/say/hi


7.2 Enable the 'rsServer' endpoint to understand JSON

	a) Include the JSON provider in Blueprint as follows:

			<bean id="jsonProvider" class="org.apache.cxf.jaxrs.provider.json.JSONProvider">
				<property name="inTransformElements">
					<map>
						<entry key="hello" value="{http://cxf.learn/}hello"/>
					</map>
				</property>
			</bean>

		Note how the property 'inTransformElements' is configured to ensure the incoming 
		JSON root element 'hello' is associated with its corresponding JAXB/XML defined namespace.

		(ref: http://cxf.apache.org/docs/jax-rs-data-bindings.html#JAX-RSDataBindings-CommonJAXBandJSONconfiguration)


	b) Add the bean reference 'jsonProvider' in the 'rsServer' endpoint, the resulting definition should be:

		<cxf:rsServer id="restvalidation"
		  address="http://localhost:20000"
		  serviceClass="learn.rest.SayResource">
			<cxf:providers>
				<ref component-id="jaxbProvider"/>
				<ref component-id="jsonProvider"/>
			</cxf:providers>
		</cxf:rsServer>


	c) Almost done, now the JSON dependencies need to be added in the POM:

		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-extension-providers</artifactId>
			<version>${cxf.version}</version>
		</dependency>
		<dependency>
			<groupId>org.codehaus.jettison</groupId>
			<artifactId>jettison</artifactId>
			<version>1.3.7</version>
		 </dependency>

		Note the 'jettison' version is set to the one installed in Fuse 6.2.1 (jettison 1.3.7).


7.3 To run standalone:

	a. run the command:
		> mvn clean camel:run

	b. from SoapUI you should be able to hit the service via the URL:
		> http://localhost:20000/say/hi

	   posting the following JSON:

		  {"hello":{"title":"Mr.","name":{"given":"Client","family":"Eastwood"}}}


7.4 Enable data validation

	Once more, our JSON endpoint definition is still not performing data validation.

	The input:

		{"hello":{"name":{"random":""}}}

  	would be accepted by the REST server but shouldn't.

	To enable data validation, redefine the bean 'jsonProvider' to include the 
	reference 'myschemas' in the property 'schemaLocations' as follows:

		<bean id="jsonProvider" class="org.apache.cxf.jaxrs.provider.json.JSONProvider">
			<property name="schemaLocations" ref="myschemas"/>
			<property name="...
			... 

	Note 'myschemas' was already defined earlier in the tutorial to enable XML validation. 
 
	Once done, then the input:

		{"hello":{"name":{"random":""}}}

  	should return the error:

		"Invalid content was found starting with element 'random'..."

	while the valid input:

		{"hello":{"title":"Mr.","name":{"given":"Client","family":"Eastwood"}}}
	
	would return:

		"Hi Mr. Client Eastwood"


7.5 Finally, some JUnits to ensure future stability of the system
	
	a. The following JUnit ensures invalid JSON is rejected

		@Test
		public void testRestJSonCxfServerComplexDataValidationRejectsInput() throws Exception
		{
			String message = null;
			try
			{	//compose and trigger the request
				String response = template.requestBodyAndHeader(
					"netty-http:http://localhost:20000/say/hi", 
					"{'hello':{'name':{'random':''}}}", 
					"Content-Type", "application/json", 
					String.class);
			}
			catch(Exception e){message = e.getMessage();};
			assertTrue("ups", message.contains("Invalid content was found starting with element 'random'"));
		}


	b. The following JUnit ensures invalid JSON is accepted

		@Test
		public void testRestJSonCxfServerComplexDataValidationAcceptsInput() throws Exception
		{
			String response = template.requestBodyAndHeader(
				"netty-http:http://localhost:20000/say/hi",
				"{'hello':{'title':'Mr.','name':{'given':'Client','family':'Eastwood'}}}", 
				"Content-Type", "application/json", 
				String.class);
			assertEquals("ups", "Hi Mr. Client Eastwood", response);
		}



8. JSON REST-Client Implementation
==================================

	This example reuses the REST-Server previously extended to accept JSON payloads.
	The new REST-Client will send JSON and the REST-Server will consume it and respond.

	The implementation starts with the creation of a JUnit.
	The Junit will trigger the Camel route that executes the CXFRS client invocation.


8.1 Add a new JUnit to test the REST-Client

		@Test
		public void testRestCxfClientComplexDataJSonDsl() throws Exception{
			String response = template.requestBody("direct:trigger-cxfrs-json-client", null, String.class);
			assertEquals("ups", "Hi Mr. Client Eastwood", response);
		}

	The Camel route triggered is defined below.


8.2 Include in Blueprint a new 'rsClient' endpoint JSON-enabled

	  <cxf:rsClient id="restjsonclient"
		 address="http://localhost:20000"
		 serviceClass="learn.rest.SayResource">
		<cxf:providers>
			<ref component-id="jsonProvider"/>
		</cxf:providers>
	  </cxf:rsClient>

	Note 'jsonProvider' was previously defined when the REST-Server was constructed.


8.3 Extend the configuration of the JSONProvider to suppress outgoing namespaces

	  <bean id="jsonProvider" class="org.apache.cxf.jaxrs.provider.json.JSONProvider">
		...
		<property name="outTransformElements">
			<map>
				<entry key="{http://cxf.learn/}hello" value="hello"/>
			</map>
		</property>

	Note how the property 'outTransformElements' is configured to ensure the outgoing 
	JSON root element 'hello' will be clean from namespaces.

	(ref: http://cxf.apache.org/docs/jax-rs-data-bindings.html#JAX-RSDataBindings-CommonJAXBandJSONconfiguration)


8.4 Add the following Camel route implementing the client invocation

	There are some preparations to do before handing over to the CXF client.
		> enable the use of 'Proxy-based API', to ensure the call is constructed based on the WADL defintion.
		> set the REST operation to invoke
		> set in the body the operation parameters (the bean 'hello')

	(refs: 
		- http://camel.apache.org/cxfrs.html
		- https://cwiki.apache.org/confluence/display/CXF20DOC/JAX-RS+Client+API#JAX-RSClientAPI-Proxy-basedAPI)

		<route id="trigger-cxfrs-json-client-complex-data">
		  <from uri="direct:trigger-cxfrs-json-client"/>
		  <setHeader headerName="CamelCxfRsUsingHttpAPI">
			<constant>false</constant>
		  </setHeader>
		  <setHeader headerName="operationName">
			<constant>postHi</constant>
		  </setHeader>
		  <setBody>
			<simple>ref:hello</simple>
		  </setBody>
		  <to uri="cxfrs:bean:restjsonclient"/>
		  <log message="client invocation succeeded: ${body}"/>
		</route>

	Note:
		- how the body is set by referencing the Bean definition 'hello'.
		- how the invocation references the newly created 'rsClient' endpoint 'restjsonclient'.


8.5 run the Junit using the maven command:

		> mvn clean test


8.6 Java Equivalent CXF JSON REST-Client

	This JUnit implements in Java the equivalent logic to the XML DSL one above.

	@Test
	public void testRestCxfClientComplexDataWithJava() throws Exception
	{
		String uri = "cxfrs:bean:restjsonclient";
		DefaultExchange request = new DefaultExchange(context);
		request.setPattern(ExchangePattern.InOut);
		request.getIn().setHeader(CxfConstants.OPERATION_NAME, "postHi");
		request.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.FALSE);
		Name name = new Name();
		name.setGiven("Client");
		name.setFamily("Eastwood");
		Hello hello = new Hello();
		hello.setTitle("Mr.");
		hello.setName(name);
		request.getIn().setBody(hello);
		Exchange response = template.send(uri, request);
		assertEquals("ups", "Hi Mr. Client Eastwood", response.getOut().getBody(String.class));
	}

	Note the URI used is equivalent to the one in XML DSL referencing the JSON 'rsClient' bean.

	The preparations are very similar to the previous XML DSL example:
		> using the 'DefaultExchange' in the JUnit we need to set the InOut pattern.
		> enable the use of 'Proxy-based API', to ensure the call is constructed based on the WADL defintion.
		> specify the REST operation to invoke
		> set in the body the JAXB object corresponding to the WADL/XSD definition




