package learn.rest;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.component.cxf.common.message.CxfConstants;

import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;

import learn.cxf.*;

import org.junit.Test;
import org.junit.Ignore;

public class RouteTest extends CamelBlueprintTestSupport {
	
    @Override
    protected String getBlueprintDescriptor() {
        return "OSGI-INF/blueprint/blueprint.xml";
    }

	@Test
	public void testRestCxfServer() throws Exception{
		String uri = "netty-http:http://localhost:10000/say/hello/junit";
		String response = template.requestBody(uri, null, String.class);
		assertEquals("ups", "Hello junit", response);
	}

	@Test
	public void testRestCxfServerComplexDataValidationRejectsInput() throws Exception
	{
		//helper variable
		String message = null;

		try
		{	//compose and trigger the request
			String response = template.requestBodyAndHeader(
				"netty-http:http://localhost:20000/say/hi", 
				"<tns:hello  xmlns:tns=\"http://cxf.learn/\"><random/></tns:hello>", 
				"Content-Type", "text/xml", 
				String.class);
		}
		catch(Exception e){message = e.getMessage();};

		//validate failure
		assertTrue("ups", message.contains("Invalid content was found starting with element 'random'"));
	}

	@Test
	public void testRestCxfServerComplexDataValidationAcceptsInput() throws Exception
	{
		//request XML
		String validXml = 
			"<tns:hello xmlns:tns=\"http://cxf.learn/\">"+
			"	<title>Mr.</title>"+
			"	<name>"+
			"		<given>Client</given>"+
			"		<family>Eastwood</family>"+
			"	</name>"+
			"</tns:hello>";

		//compose and trigger request
		String response = template.requestBodyAndHeader(
			"netty-http:http://localhost:20000/say/hi", 
			validXml, 
			"Content-Type", "text/xml", 
			String.class);

		//validate response
		assertEquals("ups", "Hi Mr. Client Eastwood", response);
	}

	@Test
	public void testRestCxfClient() throws Exception{
		String response = template.requestBody("direct:trigger-cxfrs-client", null, String.class);
		assertEquals("ups", "Hello Mr. Client Eastwood", response);
	}

	@Test
	public void testRestCxfClientWithHostPort() throws Exception
	{
		String uri = "cxfrs:http://localhost:10000?resourceClasses=learn.rest.SayResource";

		//prepare new request
		DefaultExchange request = new DefaultExchange(context);

		//set in/out pattern
        request.setPattern(ExchangePattern.InOut);

        //set the operation name 
        request.getIn().setHeader(CxfConstants.OPERATION_NAME, "getHelloqueryString");

        //using the proxy client API
        request.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.FALSE);

        // set the parameters , if you just have one parameter 
        // camel will put this object into an Object[] itself
        request.getIn().setBody("Mr. Client Eastwood");

		//trigger the request via CXF client
		Exchange response = template.send(uri, request);

		//validate response
		assertEquals("ups", "Hello Mr. Client Eastwood", response.getOut().getBody(String.class));
	}

	@Test
	public void testRestCxfClientComplexDataXmlDsl() throws Exception{
		String response = template.requestBody("direct:trigger-cxfrs-client-complex-data", null, String.class);
		assertEquals("ups", "Hi Mr. Client Eastwood", response);
	}

	@Test
	public void testRestCxfClientComplexDataWithJava() throws Exception
	{
		//URI pointing to the rsServer with XML validation
		String uri = "cxfrs:http://localhost:20000?resourceClasses=learn.rest.SayResource";

		//prepare new request
		DefaultExchange request = new DefaultExchange(context);

		//sets pattern
        request.setPattern(ExchangePattern.InOut);

        // set the operation name 
        request.getIn().setHeader(CxfConstants.OPERATION_NAME, "postHi");

        // using the proxy client API
        request.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.FALSE);

		//Payload preparation
		Name name = new Name();
		name.setGiven("Client");
		name.setFamily("Eastwood");
		Hello hello = new Hello();
		hello.setTitle("Mr.");
		hello.setName(name);

        // set the parameters , if you just have one parameter 
        // camel will put this object into an Object[] itself
        request.getIn().setBody(hello);

		//trigger the request via CXF client
		Exchange response = template.send(uri, request);

		//validate response
		assertEquals("ups", "Hi Mr. Client Eastwood", response.getOut().getBody(String.class));
	}

	@Test
	public void testRestJSonCxfServerComplexDataValidationRejectsInput() throws Exception
	{
		//helper variable
		String message = null;

		try
		{	//compose and trigger the request
			String response = template.requestBodyAndHeader(
				"netty-http:http://localhost:20000/say/hi", 
				"{'hello':{'name':{'random':''}}}", 
				"Content-Type", "application/json", 
				String.class);
		}
		catch(Exception e){message = e.getMessage();};

		//validate failure
		assertTrue("ups", message.contains("Invalid content was found starting with element 'random'"));
	}

	@Test
	public void testRestJSonCxfServerComplexDataValidationAcceptsInput() throws Exception
	{
		//compose and trigger request
		String response = template.requestBodyAndHeader(
			"netty-http:http://localhost:20000/say/hi",
			"{'hello':{'title':'Mr.','name':{'given':'Client','family':'Eastwood'}}}", 
			"Content-Type", "application/json", 
			String.class);

		//validate response
		assertEquals("ups", "Hi Mr. Client Eastwood", response);
	}


	@Test
	public void testRestCxfClientComplexDataJSonDsl() throws Exception{
		String response = template.requestBody("direct:trigger-cxfrs-json-client", null, String.class);
		assertEquals("ups", "Hi Mr. Client Eastwood", response);
	}

	@Test
	public void testRestJSonCxfClientComplexDataWithJava() throws Exception
	{
		//URI using the rsClient defined for JSON data
		String uri = "cxfrs:bean:restjsonclient";

		//prepare new request
		DefaultExchange request = new DefaultExchange(context);

		//sets pattern
        request.setPattern(ExchangePattern.InOut);

        // set the operation name 
        request.getIn().setHeader(CxfConstants.OPERATION_NAME, "postHi");

        // using the proxy client API
        request.getIn().setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.FALSE);

		//Payload preparation
		Name name = new Name();
		name.setGiven("Client");
		name.setFamily("Eastwood");
		Hello hello = new Hello();
		hello.setTitle("Mr.");
		hello.setName(name);

        // set the parameters , if you just have one parameter 
        // camel will put this object into an Object[] itself
        request.getIn().setBody(hello);

		//trigger the request via CXF client
		Exchange response = template.send(uri, request);

		//response.getException().printStackTrace();

		//validate response
		assertEquals("ups", "Hi Mr. Client Eastwood", response.getOut().getBody(String.class));
	}
}
